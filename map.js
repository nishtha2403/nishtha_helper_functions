let map = (elements,cb) => {
    let mappedArr = [];
    for(let item of elements){
        mappedArr.push(cb(item));
    }
    return mappedArr;
}

module.exports = map;