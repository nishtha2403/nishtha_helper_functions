let reduce = (elements,cb,startingValue) => {
    let accumulator;

    if(elements.length == 0 ){
        if(startingValue == undefined){
            return "Error, reduce can't work on empty array!";
        }
        return startingValue;
    }

    if(elements.length == 1 && startingValue == undefined){
        return elements[0];
    }

    if(startingValue != undefined){
        accumulator = startingValue;

        for(let i=0;i<elements.length;i++){
            accumulator = cb(accumulator,elements[i]);
        }

        return accumulator;

    }

    accumulator = elements[0];
    for(let i=1;i<elements.length;i++){
        accumulator = cb(accumulator,elements[i]);
    }

    return accumulator;

}

module.exports = reduce;