const reduce = require('../reduce.js');

const items = [1,2,3,4,5,5];

let accumulatedValue = reduce(items,(acc,curr) => acc+curr,100);

console.log(accumulatedValue);

accumulatedValue = reduce(items,(acc,curr) => acc+curr);

console.log(accumulatedValue);
