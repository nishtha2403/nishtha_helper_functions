const flatten = require('../flatten.js');

const nestedArray = [1, [2], [[3]], [[[4]]]]; 

let flatArray = flatten(nestedArray);

console.log(flatArray);