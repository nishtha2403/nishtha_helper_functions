const each = require('../each');

const items = [1,2,3,4,5,5];

each(items,(value) => {
    console.log(value);
})

each(items,(value,index) => {
    console.log(value,index)
})