const filter = require('../filter.js');

const items = [1,2,3,4,5,5];

let filterEven = filter(items,(item) => !(item%2));

console.log(filterEven);

let filterOdd = filter(items,(item) => item%2);

console.log(filterOdd);