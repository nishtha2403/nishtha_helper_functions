let filter = (elements, cb) => {
    let filteredArr = [];
    for(let item of elements){
        if(cb(item))
            filteredArr.push(item);
    }
    return filteredArr;
}

module.exports = filter;