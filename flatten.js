let flatten = elements => {
    let flatArr = [];

    for(let item of elements){
        if(typeof item == 'object'){
            flatArr.push(...flatten(item));
        }else {
            flatArr.push(item);
        }
    }

    return flatArr;

}

module.exports = flatten;